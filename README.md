# web 2019 sindominio

como objetivo para septiembre de 2019 queremos tener una web en marcha y nos iría bien poder unificar estilos y comunicación visual.

como la gente encargada de programar está acostumbrada a usar Bootstrap de https://getbootstrap.com intentaremos tener aqui una base de las hojas de estilo para que quien quiera programar lo haga sin pensar en la grafica.

## logotipo

tenemos que tener el logo disponible en varios formatos y tamaños

 * svg
 * jpg
 * png

## colores

hay que definir los colores para mensajes de suceso, error, información etc... y que de este modo sean consistentes a lo largo y ancho de la web de [SinDominio](https://sindominio.net)

## boceto homepage

en la presencial de Braojos llegamos a hacer un boceto, estoy pasandolo a html para luego presentar propuestas más desarrolladas ya que me parece que este tiene algunas dificultades de usabilidad.

![alt text] (sd-cow.jpg "propuesta portada SinDominio 2019")
